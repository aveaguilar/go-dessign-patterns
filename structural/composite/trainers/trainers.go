package trainers

import "fmt"

type Trainer interface {
	Train()
}

type Athlete struct{}

func (a *Athlete) Train() {
	fmt.Println("Training like an athlete!")
}
