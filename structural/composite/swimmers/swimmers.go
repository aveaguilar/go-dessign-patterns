package swimmers

import "fmt"

type Swimmer interface {
	Swim()
}

type FreeStyleSwimmer struct {
	Name string
}

func (b *FreeStyleSwimmer) Swim() {
	fmt.Printf("%s is swimming freestyle\n", b.Name)
}
