package main

import (
	"bitbucket.org/aveaguilar/structural/composite/binarytrees"
	"bitbucket.org/aveaguilar/structural/composite/eaters"
	"bitbucket.org/aveaguilar/structural/composite/swimmers"
	"bitbucket.org/aveaguilar/structural/composite/trainers"
)

type OlympicSwimmer struct {
	trainers.Trainer
	swimmers.Swimmer
}

type Shark struct {
	eaters.Eater
	swimmers.Swimmer
}

func main() {
	// MichaelPhelps := OlympicSwimmer{
	// 	&trainers.Athlete{},
	// 	&swimmers.FreeStyleSwimmer{
	// 		Name: "Michael Phelps",
	// 	},
	// }

	// MichaelPhelps.Train()
	// MichaelPhelps.Swim()

	// BruceTheShark := Shark{
	// 	&eaters.Animal{},
	// 	&swimmers.FreeStyleSwimmer{
	// 		Name: "Bruce the shark",
	// 	},
	// }

	// BruceTheShark.Swim()
	// BruceTheShark.Eat()

	root := binarytrees.Tree{
		Leaf: 0,
		RightBranch: &binarytrees.Tree{
			Leaf:        6,
			RightBranch: &binarytrees.Tree{7, nil, nil},
			LeftBranch:  nil,
		},
		LeftBranch: &binarytrees.Tree{5, nil, nil},
	}
	root.PrintInOrder()
}
