package eaters

import "fmt"

type Eater interface {
	Eat()
}

type Animal struct{}

func (a *Animal) Eat() {
	fmt.Println("Eating like a fish!")
}
