package binarytrees

import "fmt"

type Tree struct {
	Leaf        int
	RightBranch *Tree
	LeftBranch  *Tree
}

//PrintInOrder recursively prints the tree following In-Order
func (t *Tree) PrintInOrder() {
	if t.LeftBranch != nil {
		t.LeftBranch.PrintInOrder()
	}
	fmt.Printf("Leaf: %d\n", t.Leaf)
	if t.RightBranch != nil {
		t.RightBranch.PrintInOrder()
	}
}
