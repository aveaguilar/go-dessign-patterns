package proxy

type FinancialProduct interface {
	Withdraw(amount float32) error
}
