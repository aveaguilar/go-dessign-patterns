package proxy

import (
	"errors"
	"math/rand"
)

type BankAccount struct {
	Number int
	Owner  string
	Funds  float32
}

func newBankAccount(name string, money float32) *BankAccount {
	return &BankAccount{
		Number: rand.Int(),
		Owner:  name,
		Funds:  money,
	}
}

func (b *BankAccount) Withdraw(amount float32) error {
	if amount > b.Funds {
		return errors.New("insufficient funds")
	}
	b.Funds -= amount
	return nil
}
