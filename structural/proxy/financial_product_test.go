package proxy

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFinancialProduct_Proxy(t *testing.T) {
	myAccount := newBankAccount("John Doe", 100.00)
	card1234 := newDebitCard(myAccount)

	err := myAccount.Withdraw(50)
	assert.Nil(t, err)
	err = card1234.Withdraw(25)
	assert.Nil(t, err)
	err = card1234.Withdraw(26)
	assert.NotNil(t, err)
	assert.Equal(t, "insufficient funds", err.Error())
}
