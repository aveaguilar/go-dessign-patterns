package proxy

import "math/rand"

type DebitCard struct {
	Number  int
	Account *BankAccount
}

func newDebitCard(a *BankAccount) *DebitCard {
	return &DebitCard{
		Number:  rand.Int(),
		Account: a,
	}
}

func (d *DebitCard) Withdraw(amount float32) error {
	return d.Account.Withdraw(amount)
}
