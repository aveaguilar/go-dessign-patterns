package bridge

import (
	"fmt"
)

type PlayStation struct {
	Game Game
}

func (p *PlayStation) SetGame(g Game) {
	p.Game = g
}

func (p *PlayStation) RunGame() string {
	return fmt.Sprintf("%s on PlayStation", p.Game.Run())
}
