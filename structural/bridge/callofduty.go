package bridge

import (
	"fmt"
)

type CallOfDuty struct {
	Name string
}

func (c *CallOfDuty) Run() string {
	return fmt.Sprintf("Running Call of Duty: %s", c.Name)
}
