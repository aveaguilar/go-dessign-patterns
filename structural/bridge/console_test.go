package bridge

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConsoles(t *testing.T) {
	mw := &CallOfDuty{
		Name: "Modern Warfare",
	}
	ps3 := &PlayStation{}
	ps3.SetGame(mw)
	nowPlaying := ps3.RunGame()
	assert.Equal(t, "Running Call of Duty: Modern Warfare on PlayStation", nowPlaying)
	ds := &DarkSouls{
		Number: 2,
	}
	ps3.SetGame(ds)
	nowPlaying = ps3.RunGame()
	assert.Equal(t, "Running Dark Souls 2 on PlayStation", nowPlaying)

	xbox360 := &XBox{}
	xbox360.SetGame(mw)
	nowPlaying = xbox360.RunGame()
	assert.Equal(t, "Running Call of Duty: Modern Warfare on XBox", nowPlaying)

	xbox360.SetGame(ds)
	nowPlaying = xbox360.RunGame()
	assert.Equal(t, "Running Dark Souls 2 on XBox", nowPlaying)
}
