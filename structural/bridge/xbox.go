package bridge

import (
	"fmt"
)

type XBox struct {
	Game Game
}

func (x *XBox) SetGame(g Game) {
	x.Game = g
}

func (x *XBox) RunGame() string {
	return fmt.Sprintf("%s on XBox", x.Game.Run())
}
