package bridge

import (
	"fmt"
)

type DarkSouls struct {
	Number int
}

func (d *DarkSouls) Run() string {
	return fmt.Sprintf("Running Dark Souls %d", d.Number)
}
