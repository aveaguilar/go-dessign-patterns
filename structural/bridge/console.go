package bridge

type Console interface {
	SetGame(g Game)
	RunGame() string
}
