package bridge

type Game interface {
	Run() string
}
