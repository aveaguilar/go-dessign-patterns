package memory

import "fmt"

type Memory interface {
	Load(position int, data []byte)
}

type DDR3Memory struct{}

func (m *DDR3Memory) Load(position int, data []byte) {
	fmt.Println("Memory.Load()")
}
