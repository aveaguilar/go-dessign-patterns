package cpu

import "fmt"

type CPU interface {
	Freeze()
	Jump(position int)
	Execute()
}

type SingleCoreCPU struct{}

func (c *SingleCoreCPU) Freeze() {
	fmt.Println("CPU.Freeze()")
}

func (c *SingleCoreCPU) Jump(position int) {
	fmt.Println("CPU.Jump()")
}

func (c *SingleCoreCPU) Execute() {
	fmt.Println("CPU.Execute()")
}
