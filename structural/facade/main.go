package main

import (
	"bitbucket.org/aveaguilar/structural/facade/cpu"
	"bitbucket.org/aveaguilar/structural/facade/memory"
	"bitbucket.org/aveaguilar/structural/facade/storage"
)

const (
	BOOT_ADDRESS = 0
	BOOT_SECTOR  = 0
	SECTOR_SIZE  = 0
)

type ComputerFacade struct {
	processor cpu.CPU
	ram       memory.Memory
	hd        storage.Storage
}

func NewComputerFacade() *ComputerFacade {
	return &ComputerFacade{new(cpu.SingleCoreCPU), new(memory.DDR3Memory), new(storage.HardDrive)}
}

func (c *ComputerFacade) start() {
	c.processor.Freeze()
	c.ram.Load(BOOT_ADDRESS, c.hd.Read(BOOT_SECTOR, SECTOR_SIZE))
	c.processor.Jump(BOOT_ADDRESS)
	c.processor.Execute()
}

func main() {
	computer := NewComputerFacade()
	computer.start()
}
