package adapter

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdapter(t *testing.T) {
	msg := "Hello World!"
	adapter := PrinterAdapter{
		OldPrinter: &MyLegacyPrinter{},
		Message:    msg,
	}
	returnedMsg := adapter.PrintStored()
	assert.Equal(t, "Legacy printer: Adapter: Hello World!\n", returnedMsg)

	adapter = PrinterAdapter{OldPrinter: nil, Message: msg}
	returnedMsg = adapter.PrintStored()
	assert.Equal(t, "Hello World!", returnedMsg)
}
