package adapter

import "fmt"

type ModernPrinter interface {
	PrintStored() string
}

type PrinterAdapter struct {
	OldPrinter LegacyPrinter
	Message    string
}

func (p *PrinterAdapter) PrintStored() string {
	var newMsg string
	if p.OldPrinter != nil {
		newMsg = fmt.Sprintf("Adapter: %s", p.Message)
		return p.OldPrinter.Print(newMsg)
	}
	return p.Message
}
