package decorator

import (
	"fmt"
)

type Cellphone struct{}

func (c *Cellphone) Call() string {
	return "calling from my phone"
}

func (c *Cellphone) SendSMS(sms string) string {
	return fmt.Sprintf("Sending sms: %s", sms)
}
