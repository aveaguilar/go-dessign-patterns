package decorator

import (
	"fmt"
)

type Smartphone struct {
	Cell *Cellphone
}

func (s *Smartphone) Call() string {
	return s.Cell.Call()
}

func (s *Smartphone) Browse() string {
	return fmt.Sprintf("browsing from my smartphone")
}
