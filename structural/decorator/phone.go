package decorator

type Phone interface {
	Call() string
}
