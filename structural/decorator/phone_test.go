package decorator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPhone_Call(t *testing.T) {
	c := &Cellphone{}
	callResult := c.Call()
	assert.Equal(t, "calling from my phone", callResult)
	s := &Smartphone{
		Cell: c,
	}
	callResult = s.Cell.Call()
	assert.Equal(t, "calling from my phone", callResult)
}
