package singleton

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetInstance(t *testing.T) {
	//We delegate the creation of the object to another method (hence, the creational)
	counter1 := GetInstance()
	//We assert the creation didn't fail
	assert.NotNil(t, counter1)
	//We save our current instance for future comparison
	expectedCounter := counter1
	//We use our instance
	currentCount := counter1.Add()
	//Since it's the 1st time we use the instance, counter must be 1
	assert.Equal(t, currentCount, 1)
	//We create "another" instance
	counter2 := GetInstance()
	//We assert that the recently created instance is in fact the same we created before.
	assert.Equal(t, expectedCounter, counter2)
	//We use our instance
	currentCount = counter2.Add()
	//Since it's the 2nd time we use the instance, counter must be 2
	assert.Equal(t, currentCount, 2)
}
