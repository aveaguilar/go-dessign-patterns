package singleton

//Singleton interface
type Singleton interface {
	Add() int
}

type singleton struct {
	count int
}

var instance *singleton

//GetInstance method
func GetInstance() Singleton {
	if instance == nil {
		instance = new(singleton)
	}
	return instance
}

func (s *singleton) Add() int {
	s.count++
	return s.count
}
