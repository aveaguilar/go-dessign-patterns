package abstractfactory

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBikeFactory(t *testing.T) {
	b, err := BuildFactory(BikeFactoryType)
	assert.Nil(t, err)
	bikeVehicle, err := b.NewVehicle(SportBikeType)
	assert.Nil(t, err)
	assert.IsType(t, &SportBike{}, bikeVehicle)
}

func TestCarFactory(t *testing.T) {
	b, err := BuildFactory(CarFactoryType)
	assert.Nil(t, err)
	carVehicle, err := b.NewVehicle(FamilyCarType)
	assert.Nil(t, err)
	assert.IsType(t, &FamilyCar{}, carVehicle)
}
