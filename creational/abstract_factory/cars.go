package abstractfactory

import "errors"

type Car interface {
	NumDoors() int
}

const (
	LuxuryCarType = 1
	FamilyCarType = 2
)

type CarFactory struct{}

func (f *CarFactory) NewVehicle(v int) (Vehicle, error) {
	switch v {
	case LuxuryCarType:
		return new(LuxuryCar), nil
	case FamilyCarType:
		return new(FamilyCar), nil
	default:
		return nil, errors.New("Car type not recognized")
	}
}

type LuxuryCar struct {
}

func (n *LuxuryCar) NumDoors() int {
	return 4
}

func (n *LuxuryCar) NumWheels() int {
	return 4
}

func (n *LuxuryCar) NumSeats() int {
	return 2
}

type FamilyCar struct {
}

func (n *FamilyCar) NumDoors() int {
	return 5
}

func (n *FamilyCar) NumWheels() int {
	return 4
}

func (n *FamilyCar) NumSeats() int {
	return 5
}
