package abstractfactory

import "errors"

type Vehicle interface {
	NumWheels() int
	NumSeats() int
}

type VehicleFactory interface {
	NewVehicle(v int) (Vehicle, error)
}

const (
	CarFactoryType  = 1
	BikeFactoryType = 2
)

func BuildFactory(f int) (VehicleFactory, error) {
	switch f {
	case CarFactoryType:
		return new(CarFactory), nil
	case BikeFactoryType:
		return new(BikeFactory), nil
	default:
		return nil, errors.New("Factory not recongnized")
	}
}
