package abstractfactory

import "errors"

const (
	SportBikeType  = 1
	CruiseBikeType = 2
)

type Bike interface {
	GetBikeType() int
}

type BikeFactory struct{}

func (f *BikeFactory) NewVehicle(t int) (Vehicle, error) {
	switch t {
	case SportBikeType:
		return new(SportBike), nil
	case CruiseBikeType:
		return new(CruiseBike), nil
	default:
		return nil, errors.New("Bike type not recognized")
	}
}

type SportBike struct {
}

func (*SportBike) NumWheels() int {
	return 2
}

func (*SportBike) NumSeats() int {
	return 1
}

func (*SportBike) GetBikeType() int {
	return SportBikeType
}

type CruiseBike struct {
}

func (*CruiseBike) NumWheels() int {
	return 2
}

func (*CruiseBike) NumSeats() int {
	return 2
}

func (*CruiseBike) GetBikeType() int {
	return CruiseBikeType
}
