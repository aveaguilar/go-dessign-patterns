package prototype

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClone(t *testing.T) {
	sc := GetShirtsCloner()
	assert.NotNil(t, sc)
	item1, err := sc.GetClone(White)
	assert.Nil(t, err)
	assert.Equal(t, item1, whitePrototype)
	s1 := item1.(*Shirt)
	assert.IsType(t, &Shirt{}, s1)
	s1.SKU = "abbcc"
	item2, err := sc.GetClone(White)
	assert.Nil(t, err)
	s2 := item2.(*Shirt)
	assert.NotEqual(t, s1.SKU, s2.SKU)
	assert.NotEqual(t, s1, s2)
	t.Logf("LOG: %s", s1.GetInfo())
	t.Logf("LOG: %s", s2.GetInfo())
	t.Logf("LOG: The memory positions of the shirts are different %p != %p\n\n", &s1, &s2)
}
