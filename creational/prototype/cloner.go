package prototype

import (
	"errors"
)

type ItemCloner interface {
	GetClone(s int) (ItemInfoGetter, error)
}

func GetShirtsCloner() ItemCloner {
	return new(ShirtsCache)
}

const (
	White = 1
	Black = 2
	Blue  = 3
)

type ShirtsCache struct{}

func (sc *ShirtsCache) GetClone(s int) (ItemInfoGetter, error) {
	switch s {
	case White:
		newItem := *whitePrototype
		return &newItem, nil
	case Blue:
		newItem := *bluePrototype
		return &newItem, nil
	case Black:
		newItem := *blackPrototype
		return &newItem, nil
	default:
		return nil, errors.New("Shirt model not recognized")
	}
}
