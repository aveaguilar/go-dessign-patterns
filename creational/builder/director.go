package builder

type Director struct {
	builder BuildProcess
}

func (d *Director) Construct() ElectronicProduct {
	d.builder.SetStructure().SetMonitor().SetCamera()
	return d.builder.GetProduct()
}

func (d *Director) SetBuilder(b BuildProcess) {
	d.builder = b
}
