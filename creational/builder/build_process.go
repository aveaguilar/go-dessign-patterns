package builder

type BuildProcess interface {
	SetStructure() BuildProcess
	SetCamera() BuildProcess
	SetMonitor() BuildProcess
	GetProduct() ElectronicProduct
}
