package builder

type Smartphone struct {
	e ElectronicProduct
}

func (s *Smartphone) SetStructure() BuildProcess {
	s.e.Structure = "Smartphone"
	return s
}

func (s *Smartphone) SetMonitor() BuildProcess {
	s.e.Monitor = 1
	return s
}

func (s *Smartphone) SetCamera() BuildProcess {
	s.e.Camera = 2
	return s
}

func (s *Smartphone) GetProduct() ElectronicProduct {
	return s.e
}
