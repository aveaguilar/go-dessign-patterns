package builder

type ElectronicProduct struct {
	Structure string
	Monitor   int
	Camera    int
}
