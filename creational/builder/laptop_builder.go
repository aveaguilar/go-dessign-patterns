package builder

type Laptop struct {
	e ElectronicProduct
}

func (l *Laptop) SetStructure() BuildProcess {
	l.e.Structure = "Laptop"
	return l
}

func (l *Laptop) SetMonitor() BuildProcess {
	l.e.Monitor = 1
	return l
}

func (l *Laptop) SetCamera() BuildProcess {
	l.e.Camera = 1
	return l
}

func (l *Laptop) GetProduct() ElectronicProduct {
	return l.e
}
