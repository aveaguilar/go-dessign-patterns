package builder

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuilderPattern(t *testing.T) {
	director := Director{}
	laptopBuilder := &Laptop{}
	director.SetBuilder(laptopBuilder)
	director.Construct()
	laptop := laptopBuilder.GetProduct()
	assert.Equal(t, laptop.Monitor, 1)
	assert.Equal(t, laptop.Camera, 1)
	assert.Equal(t, laptop.Structure, "Laptop")
	smartphoneBuilder := &Smartphone{}
	director.SetBuilder(smartphoneBuilder)
	director.Construct()
	smartphone := smartphoneBuilder.GetProduct()
	assert.Equal(t, smartphone.Monitor, 1)
	assert.Equal(t, smartphone.Camera, 2)
	assert.Equal(t, smartphone.Structure, "Smartphone")
}
