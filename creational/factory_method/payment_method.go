package factory

import "errors"

type PaymentMethod interface {
	Pay(total float32) string
}

const (
	Cash = 1
	Card = 2
)

func GetPaymentMethod(m int) (PaymentMethod, error) {
	switch m {
	case Cash:
		pm := new(CashPM)
		return pm, nil
	case Card:
		pm := new(CardPM)
		return pm, nil
	default:
		return nil, errors.New("Not implemented")
	}
}

type CashPM struct{}

type CardPM struct{}

func (c *CashPM) Pay(total float32) string {
	return "paid using cash"
}

func (c *CardPM) Pay(total float32) string {
	return "paid using card"
}
