package factory

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreatePaymentMethod_Cash(t *testing.T)  {
	p, err := GetPaymentMethod(Cash)
	assert.Nil(t, err)
	msg := p.Pay(4.20)
	assert.Equal(t, msg, "paid using cash")
}

func TestCreatePaymentMethod_Card(t *testing.T)  {
	p, err := GetPaymentMethod(Card)
	assert.Nil(t, err)
	msg := p.Pay(4.20)
	assert.Equal(t, msg, "paid using card")
}

func TestCreatePaymentMethod_NotDefined(t *testing.T)  {
	_, err := GetPaymentMethod(4)
	assert.NotNil(t, err)
	assert.Equal(t, err.Error(), "Not implemented")
}