package iterator

type Collection interface {
	createIterator() Iterator
}

type TaxonomyCollection struct {
	taxonomies []Taxonomy
}

func (tc *TaxonomyCollection) createIterator() Iterator {
	return &TaxonomyIterator{
		taxonomies: tc.taxonomies,
	}
}

type Taxonomy interface {
	getGenus() string
	getSpecies() string
}

type ShortTaxonomy struct {
	Genus   string
	Species string
}

func (st *ShortTaxonomy) getGenus() string {
	return st.Genus
}

func (st *ShortTaxonomy) getSpecies() string {
	return st.Species
}
