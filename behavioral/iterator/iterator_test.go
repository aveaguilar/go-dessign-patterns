package iterator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIterator(t *testing.T) {
	species := []string{
		"leo",
		"tigris",
		"lupus",
	}
	genuses := []string{
		"Phantera",
		"Canis",
	}
	lion := &ShortTaxonomy{
		Genus:   "Phantera",
		Species: "leo",
	}

	tiger := &ShortTaxonomy{
		Genus:   "Phantera",
		Species: "tigris",
	}

	wolf := &ShortTaxonomy{
		Genus:   "Canis",
		Species: "lupus",
	}
	col := []Taxonomy{lion, tiger, wolf}
	taxonomies := &TaxonomyCollection{
		taxonomies: col,
	}
	iter := taxonomies.createIterator()
	for iter.hasNext() {
		animal := iter.next()
		assert.Contains(t, genuses, animal.getGenus())
		assert.Contains(t, species, animal.getSpecies())
		t.Logf("%s %s", animal.getGenus(), animal.getSpecies())
	}
}
