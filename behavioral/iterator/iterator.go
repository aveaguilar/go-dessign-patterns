package iterator

type Iterator interface {
	hasNext() bool
	next() Taxonomy
}

type TaxonomyIterator struct {
	index      int
	taxonomies []Taxonomy
}

func (ti *TaxonomyIterator) hasNext() bool {
	if ti.index < len(ti.taxonomies) {
		return true
	}
	return false
}

func (ti *TaxonomyIterator) next() Taxonomy {
	if ti.hasNext() {
		t := ti.taxonomies[ti.index]
		ti.index++
		return t
	}
	return nil
}
