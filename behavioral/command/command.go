package command

type Command interface {
	execute() string
}
