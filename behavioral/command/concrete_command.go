package command

type FeedCommand struct {
	food  string
	myPet Pet
}

func (fc *FeedCommand) execute() string {
	return fc.myPet.Eat(fc.food)
}

func (fc *FeedCommand) setFood(foodType string) {
	fc.food = foodType
}
