package command

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCommand(t *testing.T) {
	rocky := &Dog{
		Name: "Rocky",
	}
	feed := &FeedCommand{
		myPet: rocky,
	}
	feed.setFood("steak")
	john := &PetOwner{
		command: feed,
	}
	result := john.FeedPet()
	assert.Equal(t, "The doggo is eating steak", result)
}
