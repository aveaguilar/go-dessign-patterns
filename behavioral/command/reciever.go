package command

import (
	"fmt"
)

type Pet interface {
	Eat(string) string
}

type Dog struct {
	Name string
}

func (d *Dog) Eat(food string) string {
	return fmt.Sprintf("The doggo is eating %s", food)
}
