package command

type PetOwner struct {
	command Command
}

func (po *PetOwner) FeedPet() string {
	return po.command.execute()
}
