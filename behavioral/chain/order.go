package chain

import (
	"fmt"
	"math/rand"
)

type Order struct {
	ID     string
	Status OrderStatus
}

func newOrder() *Order {
	return &Order{
		ID:     fmt.Sprintf("%d", rand.Int()),
		Status: CREATED,
	}
}

const (
	CREATED    OrderStatus = 1
	IN_PROCESS OrderStatus = 2
	IN_TRANSIT OrderStatus = 3
	DELIVERED  OrderStatus = 4
)

type OrderStatus int

func (s OrderStatus) ToInt() int {
	return int(s)
}
