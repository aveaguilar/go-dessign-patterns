package chain

type CompleteOrder struct {
	next Handler
}

func (co *CompleteOrder) UpdateStatus(o *Order) {
	println("Setting order completed")
	o.Status = DELIVERED
	if co.next != nil {
		co.next.UpdateStatus(o)
	}
}

func (co *CompleteOrder) SetNext(h Handler) {
	co.next = h
}
