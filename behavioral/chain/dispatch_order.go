package chain

type DispatchOrder struct {
	next Handler
}

func (do *DispatchOrder) UpdateStatus(o *Order) {
	println("Setting order in transit")
	o.Status = IN_TRANSIT
	if do.next != nil {
		do.next.UpdateStatus(o)
	}
}

func (do *DispatchOrder) SetNext(h Handler) {
	do.next = h
}
