package chain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestChain_Completed(t *testing.T) {
	order := newOrder()
	completer := &CompleteOrder{}
	dispatcher := &DispatchOrder{}
	dispatcher.SetNext(completer)
	chain := &ProcessOrder{}
	chain.SetNext(dispatcher)
	chain.UpdateStatus(order)
	assert.Equal(t, 4, order.Status.ToInt())
}

func TestChain_InTransit(t *testing.T) {
	order := newOrder()
	dispatcher := &DispatchOrder{}
	chain := &ProcessOrder{}
	chain.SetNext(dispatcher)
	chain.UpdateStatus(order)
	assert.Equal(t, 3, order.Status.ToInt())
}

func TestChain_InProcess(t *testing.T) {
	order := newOrder()
	chain := &ProcessOrder{}
	chain.UpdateStatus(order)
	assert.Equal(t, 2, order.Status.ToInt())
}
