package chain

type Handler interface {
	UpdateStatus(o *Order)
	SetNext(Handler)
}
