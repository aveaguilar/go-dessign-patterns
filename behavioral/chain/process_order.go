package chain

type ProcessOrder struct {
	next Handler
}

func (po *ProcessOrder) UpdateStatus(o *Order) {
	println("Setting order in process")
	o.Status = IN_PROCESS
	if po.next != nil {
		po.next.UpdateStatus(o)
	}
}

func (po *ProcessOrder) SetNext(h Handler) {
	po.next = h
}
