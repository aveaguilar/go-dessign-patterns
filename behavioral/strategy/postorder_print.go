package strategy

import (
	"fmt"
	"strings"
)

type PostOrderPrinter struct {
}

func (p *PostOrderPrinter) PrintTree(t *Tree) string {
	var result strings.Builder
	if t.LeftBranch != nil {
		result.WriteString(p.PrintTree(t.LeftBranch))
	}
	if t.RightBranch != nil {
		result.WriteString(p.PrintTree(t.RightBranch))
	}
	result.WriteString(fmt.Sprintf("[%d]", t.Leaf))
	return result.String()
}
