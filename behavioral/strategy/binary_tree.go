package strategy

type Tree struct {
	Leaf        int
	RightBranch *Tree
	LeftBranch  *Tree
}
