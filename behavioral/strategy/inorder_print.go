package strategy

import (
	"fmt"
	"strings"
)

type InOrderPrinter struct {
}

func (i *InOrderPrinter) PrintTree(t *Tree) string {
	var result strings.Builder
	if t.LeftBranch != nil {
		result.WriteString(i.PrintTree(t.LeftBranch))
	}
	result.WriteString(fmt.Sprintf("[%d]", t.Leaf))
	if t.RightBranch != nil {
		result.WriteString(i.PrintTree(t.RightBranch))
	}
	return result.String()
}
