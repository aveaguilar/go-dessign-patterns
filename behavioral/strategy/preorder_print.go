package strategy

import (
	"fmt"
	"strings"
)

type PreOrderPrinter struct {
}

func (p *PreOrderPrinter) PrintTree(t *Tree) string {
	var result strings.Builder
	result.WriteString(fmt.Sprintf("[%d]", t.Leaf))
	if t.LeftBranch != nil {
		result.WriteString(p.PrintTree(t.LeftBranch))
	}
	if t.RightBranch != nil {
		result.WriteString(p.PrintTree(t.RightBranch))
	}
	return result.String()
}
