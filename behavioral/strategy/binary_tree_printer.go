package strategy

type BinaryTreePrinter interface {
	PrintTree(t *Tree) string
}

type Printer struct {
	Printer BinaryTreePrinter
}

func newPrinter() *Printer {
	return &Printer{}
}

func (p *Printer) SetPrintOrder(s BinaryTreePrinter) {
	p.Printer = s
}
