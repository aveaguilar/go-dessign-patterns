package strategy

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var root = &Tree{
	Leaf: 0,
	RightBranch: &Tree{
		Leaf:        6,
		RightBranch: &Tree{7, nil, nil},
		LeftBranch:  nil,
	},
	LeftBranch: &Tree{5, nil, nil},
}

func TestBinaryTreePrinter_PreOrder(t *testing.T) {
	printer := newPrinter()
	preOrder := &PreOrderPrinter{}
	printer.SetPrintOrder(preOrder)
	s := printer.Printer.PrintTree(root)
	assert.Equal(t, "[0][5][6][7]", s)
}

func TestBinaryTreePrinter_InOrder(t *testing.T) {
	printer := newPrinter()
	inOrder := &InOrderPrinter{}
	printer.SetPrintOrder(inOrder)
	s := printer.Printer.PrintTree(root)
	assert.Equal(t, "[5][0][6][7]", s)
}

func TestBinaryTreePrinter_PostOrder(t *testing.T) {
	printer := newPrinter()
	postOrder := &PostOrderPrinter{}
	printer.SetPrintOrder(postOrder)
	s := printer.Printer.PrintTree(root)
	assert.Equal(t, "[5][7][6][0]", s)
}
